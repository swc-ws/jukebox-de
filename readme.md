# Isoliertes Testing mit Stubs und Mocks

In diesem Projekt finden Sie eine sehr einfache Implementation eines Musicplayers - in Anlehnung an dessen physischen Vorgänger [Jukebox](https://de.wikipedia.org/wiki/Jukebox) genannt. Eine konkrete Implementation des `Song`-Interfaces existiert noch nicht. 

Trotzdem möchte die Entwicklerin Maude ihre Implementierung der JukeBox testen und hat sich für folgende Lösung entschieden.

![Klassendiagramm für Stubbing](./doc/stub-diagram.png)

## Aufgabe 1 - Testen mit Stubs

In dieser ersten Aufgabe werden Sie das Testen mit selbst geschriebenen Stubklassen üben. Die Klasse `JukeBoxStubTest` wurde schon zur Verfügung gestellt, jedoch existiert noch keine Klasse `SongStub`.

*Achtung: Wenn Sie das Maven-Projekt importieren/laden, wird es (noch) nicht kompileren, da die SongStub-Klasse noch fehlt.*

### Einführung

In dieser Testklasse sind vier Tests programmiert.
1.	`testPlayOfNonexistingSong` versucht einen Song zu spielen, den es nicht gibt. Es wird eine Exception von der JukeBox erwartet.
2.	`testGetPlayList` fügt einen Song der JukeBox hinzu und testet dann ob die Playlist nicht leer ist.
3.	`testPlay` fügt einen Song der JukeBox hinzu und spielt diesen ab. Zudem wird der aktuelle Song geholt und überprüft ob er existiert und ob er gerade abgespielt wird.
4.	`testPlayOfAlreadyPlayingSong` fügt einen Song der JukeBox hinzu und startet diesen zweimal unmittelbar hintereinander. Beim zweiten Start wird eine Exception vom Song erwartet.

### Aufgaben

1.	Implementieren Sie eine Klasse `SongStub` so, dass alle Methoden einen festen (=konstanten) Wert zurückgeben. Können durch geschickte Wahl der Rückgabewerte überhaupt alle Tests grün werden?

   *Bevor Sie beginnen: In welches Verzeichnis gehört die Klasse SongStub.java?*

2.	Schreiben Sie die Klasse `SongStub` so, dass alle 4 Tests grün werden. Die Implementation soll minimalistisch sein. Ist es nötig Zustände in `SongStub` zu speichern? Falls ja, welche? Falls nein, implementieren Sie die Klasse `SongStub` zustandslos.


## Aufgabe 2 - Testen mit Mocks

Stubs sind nicht sehr intelligent und daher kann man häufig nur oberflächlich mit Stubs testen. Ansonsten kann das Testen mit Stubs sehr aufwendig werden. Für das vollständige Testen verwendet man besser Mock-Objekte. Die folgenden Aufgaben illustrieren dies.

Sie werden mit dem Mockito-Framework arbeiten. Daher müssen Sie zunächst die neueste stabile Version des Mockito-Frameworks in den dependencies des POM-Files konfigurieren.

Wenn die Methode `playSong()` auf einem JukeBox-Objekt aufgerufen wird, erwartet man, dass dieses zuerst nach dem Song sucht und ihn danach startet. Dies bedeutet auf dem gesuchten Song erwarten wir mindestens einen Aufruf der Methode `playSong()` (nämlich beim Holen des Song-Objektes aus der Liste) und `start()` (beim Abspielen).


### Aufgaben

1.	Erstellen Sie zunächst eine Klasse `JukeBoxMockTest`, mit der Sie die folgenden Tests implementieren.

2.	Implementieren Sie eine neue `testActualSong`-Methode in welcher Sie ein Mock-Song-Objekt erzeugen und mit diesem Testen ob die erwarteten Aufrufe auch tatsächlich stattfinden. D.h. Sie sollen hier das Beispiel aus den Folien selber nachvollziehen.

3.	Implementieren Sie eine Methode `testGetPlayList()` in der Sie überprüfen, dass ein Song maximal nur einmal in der Playlist landet, auch wenn er mehrfach mit `addSong()` hinzugefügt wurde.

4.	Implementieren Sie eine Methode `testPlayOfAlreadyPlayingSong()` mit welcher sie überprüfen ob tatsächlich eine Exception geworfen wird, falls auf einem Song der gerade abgespielt wird, nochmals die `playSong()`-Methode mit demselben Songtitel aufgerufen wird.
	
   Stellen Sie zudem sicher, dass die `start()` auf dem Song überhaupt aufgerufen wurde.
	
5.	Erstellen Sie nun eine Methode `testPlayTitle()` und lösen Sie folgendes Problem. Wie stellen Sie sicher, dass das zu testende JukeBox-Objekt die Methoden des Song-Objektes in der richtigen Reihenfolge aufruft. 


## Aufgabe 3 - Klassenentwurf für Testisolation

Bisher werden die Songs einfach über die `addSong()`-Methode zur Jukebox hinzugefügt. Doch mit dem Aufkommen von Streamingdiensten ist die Idee entstanden, Songs aus dem Internet herunterzuladen und der MusicJukeBox hinzuzufügen. Ein erster Entwurf einer Implementation stellt Entwickler Richard zur Verfügung:

```java
public class SpotifyJukeBoxLoader {
    private SongService service = new SpotifySongService();
    private JukeBox jukebox = new MusicJukeBox();
    
    public SpotifyJukeBoxLoader(String spotifyID){
        for (Song song: service.getPlaylist(spotifyID)) {
            jukebox.addSong(song);
        }
    }
}
```

Die Klasse `SpotifySongService` soll bei Spotify eine Playlist herunterladen und die Songs unserer Jukebox zur Verfügung stellen.

### Aufgaben
1. Sie erkennen schnell das Problem, welches beim isolierten Testen von `SpotifyJukeBoxLoader` entsteht. Erklären Sie es!
2. Wie könnten Sie die Klasse `SpotifyJukeBoxLoader` trotzdem testen? Machen Sie Vorschläge zuhanden Richards wie er den Code ändern sollte um einen Test in Isolation zu ermöglichen.

Die Implementation sei den geneigten Studierenden überlassen :-)
Dazu sollte auch das [Web-API von Spotify](https://developer.spotify.com/documentation/web-api/) konsultiert werden.


##  Aufgabe 4 - Vertiefen Sie Ihre Mockito Kenntnisse (fakultativ)
Bearbeiten Sie die Tutorials 1-3 und 5 unter https://www.softwaretestinghelp.com/mockito-tutorial/  

Bemerkungen:
- Wir werden nicht selbst mit Spys arbeiten, Sie sollten aber deren Verwendungszweck kennen.
- Falls Sie je das Bedürfnis verspüren private oder statische Methoden zu mocken, dann lesen Sie die ersten drei Absätze von Tutorial 4:

```
If the need arises to mock private and static methods/classes, it indicates 
poorly refactored code and is not really a testable code and is most likely 
that some legacy code which was not used to be very unit test friendly.
```



### Weitere Resourcen

Martin Fowler über Mocks und Stubs: [https://martinfowler.com/articles/mocksArentStubs.html](https://martinfowler.com/articles/mocksArentStubs.html)

Mockito Kurztutorial: [https://www.vogella.com/tutorials/Mockito/article.html](https://www.vogella.com/tutorials/Mockito/article.html)

Mockito Tutorials bis zum Abwinken: [https://www.javacodegeeks.com/mockito-tutorials](https://www.javacodegeeks.com/mockito-tutorials)

